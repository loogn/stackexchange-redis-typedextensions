﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

using StackExchange.Redis;

namespace RedisTest
{

    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            using (var conn = ConnectionMultiplexer.Connect("192.168.1.110:6379"))
            {
                var db = conn.GetDatabase();
                //list
                db.ListRightPush("users", new User { ID = 1, Name = "a" });
                db.ListRightPush("users", new User { ID = 2, Name = "b" });
                db.ListRightPush("users", new User { ID = 3, Name = "c" });
                Console.WriteLine(db.ListLength(CacheKeys.users));
                User[] users = db.ListRange<User>(CacheKeys.users, 0, 2);
                users.PrintDump();

                //Hash
                db.HashSet<User>(CacheKeys.HashKey, "1", new User { ID = 1 });
                db.HashSet<User>(CacheKeys.HashKey, "2", new User { ID = 2 });
                db.HashSet<User>(CacheKeys.HashKey, "3", new User { ID = 3 });
                var user3 = db.HashGet<User>(CacheKeys.HashKey, "3");
                user3.PrintDump();

                //Set
                db.SetAdd<User>(CacheKeys.SetKey, new User { ID = 23, Name = "名称23" });
                db.SetAdd<User>(CacheKeys.SetKey, new User { ID = 13, Name = "名称13" });
                var userRandom = db.SetRandomMember<User>(CacheKeys.SetKey);
                userRandom.PrintDump();

                //SortedSet
                db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = 1, Name = "name1" }, 1);
                db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = 3, Name = "Log3" }, 5);
                db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = 2, Name = "Log2" }, 2);
                db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = -1, Name = "Log-1" }, -1);
                var userArr = db.SortedSetRangeByScoreWithScores<User>(CacheKeys.SortedSetKey);
                userArr.PrintDump();

            }
        }
    }
}
