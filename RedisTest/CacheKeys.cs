﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisTest
{
    class CacheKeys
    {
        public static readonly UnionCacheKey users = new UnionCacheKey("users");

        public static readonly UnionCacheKey user = new UnionCacheKey("user_{0}");

        public static readonly UnionCacheKey HashKey = new UnionCacheKey("hash_User");

        public static readonly UnionCacheKey SetKey = new UnionCacheKey("set_User");

        public static readonly UnionCacheKey SortedSetKey = new UnionCacheKey("sortedSet_User");

    }
}
