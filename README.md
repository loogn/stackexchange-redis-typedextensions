# StackExchange.Redis.TypedExtensions

StackExchange.Redis客户端扩展，支持List、Hash、Set、SortedSet类型中直接使用泛型。   
和StackExchange.Redis自身的方法签名保持完全一致。


```
    static void Main(string[] args)
    {
        using (var conn = ConnectionMultiplexer.Connect("127.0.0.1:6379"))
        {
            var db = conn.GetDatabase();
            //list
            db.ListRightPush("users", new User { ID = 1, Name = "a" });
            db.ListRightPush("users", new User { ID = 2, Name = "b" });
            db.ListRightPush("users", new User { ID = 3, Name = "c" });
            Console.WriteLine(db.ListLength(CacheKeys.users));
            User[] users = db.ListRange<User>(CacheKeys.users, 0, 2);
            foreach (var item in users)
            {
                Console.WriteLine("ID:{0},Name:{1}", item.ID, item.Name);
            }

            //Hash
            db.HashSet<User>(CacheKeys.HashKey, "1", new User { ID = 1 });
            db.HashSet<User>(CacheKeys.HashKey, "2", new User { ID = 2 });
            db.HashSet<User>(CacheKeys.HashKey, "3", new User { ID = 3 });
            var user3 = db.HashGet<User>(CacheKeys.HashKey, "3");
            Console.WriteLine("ID:{0},Name:{1}", user3.ID, user3.Name);

            //Set
            db.SetAdd<User>(CacheKeys.SetKey, new User { ID = 23, Name = "名称23" });
            db.SetAdd<User>(CacheKeys.SetKey, new User { ID = 13, Name = "名称13" });
            var userRandom = db.SetRandomMember<User>(CacheKeys.SetKey);
            Console.WriteLine("ID:{0},Name:{1}", userRandom.ID, userRandom.Name);
            
            //SortedSet
            db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = 1, Name = "name1" }, 1);
            db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = 3, Name = "Log3" }, 5);
            db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = 2, Name = "Log2" }, 2);
            db.SortedSetAdd(CacheKeys.SortedSetKey, new User { ID = -1, Name = "Log-1" }, -1);
            var userArr = db.SortedSetRangeByScoreWithScores<User>(CacheKeys.SortedSetKey);
            foreach (var item in userArr)
            {
                Console.WriteLine("ID:{0},Name:{1},score:{2}", item.Element.ID, item.Element.Name, item.Score);
            }
        }
    }


```
