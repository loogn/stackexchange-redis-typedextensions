﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackExchange.Redis
{
    /// <summary>
    /// 统一Key,带参数和不带参数的key
    /// </summary>
    public class UnionCacheKey
    {
        string _key;
        public UnionCacheKey(string key)
        {
            _key = key;
        }
        public override string ToString()
        {
            if (_key.IndexOf("{0}") >= 0)
            {
                throw new Exception(_key + "需要额外参数，请调用BuildWithParams设置");
            }
            return _key;
        }

        public UnionCacheKey BuildWithParams(params object[] args)
        {
            if (args.Length == 0)
            {
                throw new Exception("如果没有参数，请不要调用BuildWithParams");
            }
            var m = new ParamsCacheKey(_key, args);
            return m;
        }

        class ParamsCacheKey : UnionCacheKey
        {

            object[] _args;
            public ParamsCacheKey(string key, object[] args) : base(key)
            {
                _args = args;
            }

            public override string ToString()
            {
                return string.Format(_key, _args);
            }

        }

        public static implicit operator RedisKey(UnionCacheKey ucKey)
        {
            RedisKey rk = ucKey.ToString();
            return rk;
        }
    }
}
